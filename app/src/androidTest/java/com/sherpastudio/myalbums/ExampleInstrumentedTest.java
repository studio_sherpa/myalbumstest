package com.sherpastudio.myalbums;

import android.os.Handler;
import android.os.Looper;

import com.sherpastudio.myalbums.old.data.AlbumModel;
import com.sherpastudio.myalbums.old.data.AlbumsRepository;
import com.sherpastudio.myalbums.old.view.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.test.core.app.ActivityScenario;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import static com.sherpastudio.myalbums.utils.TestUtils.getCountFromRecyclerView;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Before
    public void prepare(){
        ActivityScenario.launch(MainActivity.class);
    }

    @Test
    public void simpleTest() throws InterruptedException {

        final AtomicInteger numberItems = new AtomicInteger(0);

        CountDownLatch latch = new CountDownLatch(1);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                LiveData<List<AlbumModel>> allAlbums = AlbumsRepository.getInstance().getAllAlbums();
                allAlbums.observeForever(new Observer<List<AlbumModel>>() {
                    @Override
                    public void onChanged(List<AlbumModel> albumModels) {
                        if(albumModels == null) return;
                        numberItems.set(albumModels.size());
                        allAlbums.removeObserver(this);
                        latch.countDown();
                    }
                });
            }
        });

        //Wait until the latch is ready (we have albums) with a timeout of 1 minute (enough even
        //for a slow connection
        latch.await(1, TimeUnit.MINUTES);

        int count = getCountFromRecyclerView(R.id.album_list);

        assertEquals(numberItems.get(), count);
    }

}
