package com.sherpastudio.myalbums.modules.view;

import com.sherpastudio.myalbums.modules.domain.UseCaseGetAllModels;
import com.sherpastudio.myalbums.modules.domain.base.UseCaseCallback;
import com.sherpastudio.myalbums.modules.domain.base.UseCaseHandler;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ModulesViewModel extends ViewModel {

    private final UseCaseHandler useCaseHandler;
    private final UseCaseGetAllModels getAllModels;

    private MutableLiveData<List<ModuleItem>> observableItems;


    public ModulesViewModel(UseCaseHandler useCaseHandler, UseCaseGetAllModels getAllModels) {
        this.useCaseHandler = useCaseHandler;
        this.getAllModels = getAllModels;

        observableItems = new MutableLiveData<>();
    }


    public LiveData<List<ModuleItem>> getAllItems() {
        return observableItems;
    }

    public void refreshModules() {
        UseCaseGetAllModels.RequestValues requestValue = new UseCaseGetAllModels.RequestValues();


        useCaseHandler.execute(getAllModels, requestValue, new UseCaseCallback<UseCaseGetAllModels.ResponseValue>() {
            @Override
            public void onSuccess(UseCaseGetAllModels.ResponseValue response) {
                observableItems.postValue(response.list);
            }

            @Override
            public void onError(Throwable t) {
                //TODO do somehting
            }
        });

    }

}
