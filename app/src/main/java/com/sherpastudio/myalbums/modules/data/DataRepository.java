package com.sherpastudio.myalbums.modules.data;

import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.List;

import androidx.annotation.NonNull;

public class DataRepository implements IDataRepository {
    private static DataRepository INSTANCE;


    private final ILocalRepository localRepository;
    private final IRemoteDataRepository remoteRepository;
    private boolean isCacheDirty;

    public static DataRepository getInstance(@NonNull ILocalRepository localRepository,
                                             @NonNull IRemoteDataRepository remoteDataRepository) {
        if(INSTANCE == null){
            INSTANCE = new DataRepository(localRepository, remoteDataRepository);
        }
        return INSTANCE;
    }

    private DataRepository(@NonNull ILocalRepository localRepository,
                           @NonNull IRemoteDataRepository remoteDataRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteDataRepository;
        this.isCacheDirty = true;
    }

    @Override
    public void getAllModules(@NonNull IDataCallbackGetAllModules callback) {
        if (isCacheDirty) {
            getModulesFromServer(callback);
        } else {
            getModulesFromLocal(callback);
        }

    }

    private void getModulesFromServer(IDataCallbackGetAllModules callback) {
        remoteRepository.getAllModules(new IDataCallbackGetAllModules() {
            @Override
            public void onSuccess(List<Module> modules) {
                refreshCache();
                refreshLocalCache(modules);
                callback.onSuccess(modules);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onError(throwable);
            }
        });

    }

    private void refreshLocalCache(List<Module> modules) {
        localRepository.saveModules(modules);
    }

    private void getModulesFromLocal(IDataCallbackGetAllModules callback) {
        localRepository.getAllModules(new IDataCallbackGetAllModules() {
            @Override
            public void onSuccess(List<Module> modules) {
                refreshCache();
                callback.onSuccess(modules);
            }

            @Override
            public void onError(Throwable throwable) {
                getModulesFromServer(callback);
            }
        });
    }

    private void refreshCache() {
        isCacheDirty = false;
    }
}
