package com.sherpastudio.myalbums.modules.domain.base;

public interface UseCaseCallback<R> {
    void onSuccess(R response);
    void onError(Throwable t);
}
