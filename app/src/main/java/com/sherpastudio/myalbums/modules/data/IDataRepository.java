package com.sherpastudio.myalbums.modules.data;

import androidx.annotation.NonNull;

public interface IDataRepository {
    void getAllModules(@NonNull IDataCallbackGetAllModules callback);


}
