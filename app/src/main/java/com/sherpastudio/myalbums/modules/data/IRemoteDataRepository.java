package com.sherpastudio.myalbums.modules.data;

import androidx.annotation.NonNull;

public interface IRemoteDataRepository {
    void getAllModules(@NonNull IDataCallbackGetAllModules callback);
}
