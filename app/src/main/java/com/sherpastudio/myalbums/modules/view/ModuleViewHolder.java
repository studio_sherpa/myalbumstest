package com.sherpastudio.myalbums.modules.view;

import android.view.View;
import android.widget.TextView;

import com.sherpastudio.myalbums.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ModuleViewHolder extends RecyclerView.ViewHolder{

    public TextView nameView;

    public ModuleViewHolder(@NonNull View itemView) {
        super(itemView);
        nameView = itemView.findViewById(R.id.album_title);
    }
}
