package com.sherpastudio.myalbums.modules.data;


import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.List;

import androidx.annotation.NonNull;

public interface ILocalRepository {
    void getAllModules(@NonNull IDataCallbackGetAllModules callback);

    void saveModules(List<Module> modules);
}
