package com.sherpastudio.myalbums.modules.view;

import com.sherpastudio.myalbums.modules.data.model.Module;

public class ModuleItem {
    private final Module module;
    private boolean selected;

    public ModuleItem(Module module, boolean selected) {
        this.module = module;
        this.selected = selected;
    }

    boolean isItemTheSame(ModuleItem newItem) {
        return false;
    }

    boolean isContentTheSame(ModuleItem newItem) {
        return false;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getName() {
        return module.getTitle();
    }
}
