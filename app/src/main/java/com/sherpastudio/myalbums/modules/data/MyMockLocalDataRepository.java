package com.sherpastudio.myalbums.modules.data;

import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class MyMockLocalDataRepository implements ILocalRepository {
    private static final MyMockLocalDataRepository ourInstance = new MyMockLocalDataRepository();

    private List<Module> mModules;

    public static MyMockLocalDataRepository getInstance() {
        return ourInstance;
    }

    private MyMockLocalDataRepository() {
        mModules = new ArrayList<>();
    }


    @Override
    public void getAllModules(@NonNull IDataCallbackGetAllModules callback) {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.onSuccess(mModules);
    }

    @Override
    public void saveModules(List<Module> modules) {
        mModules = modules;
    }
}
