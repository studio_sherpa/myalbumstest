package com.sherpastudio.myalbums.modules.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sherpastudio.myalbums.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


public class ModuleListAdapter extends RecyclerView.Adapter<ModuleViewHolder> {

    private List<ModuleItem> mItemList;

    public ModuleListAdapter() {
        mItemList = new ArrayList<>();
    }

    public void setModules(final List<ModuleItem> newModules){
        if(mItemList == null) {
            mItemList = newModules;
            notifyItemRangeInserted(0, newModules.size());
        }
        else{
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mItemList.size();
                }

                @Override
                public int getNewListSize() {
                    return newModules.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    ModuleItem oldItem =  mItemList.get(oldItemPosition);
                    ModuleItem newItem =  newModules.get(newItemPosition);
                    return oldItem.isItemTheSame(newItem);
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {

                    ModuleItem oldItem =  mItemList.get(oldItemPosition);
                    ModuleItem newItem =  newModules.get(newItemPosition);
                    return oldItem.isContentTheSame(newItem);
                }
            });
            mItemList = newModules;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public ModuleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ModuleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ModuleViewHolder holder, int position) {
        holder.nameView.setText(mItemList.get(position).getName());
        holder.itemView.setOnClickListener(v -> {
//            ListAlbumsFragmentDirections.ActionListAlbumsFragmentToAlbumPreviewFragment action =
//                    ListAlbumsFragmentDirections.actionListAlbumsFragmentToAlbumPreviewFragment();
//            action.setAlbumId(mItemList.get(position).getId());
//            Navigation.findNavController(v).navigate(action);
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
