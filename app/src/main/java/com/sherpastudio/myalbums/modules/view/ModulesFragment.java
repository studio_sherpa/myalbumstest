package com.sherpastudio.myalbums.modules.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sherpastudio.myalbums.R;
import com.sherpastudio.myalbums.modules.Injection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ModulesFragment extends Fragment {

    private ModulesViewModel mViewModel;
    private ModuleListAdapter mAdapter;

    public static ModulesFragment newInstance() {
        return new ModulesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.modules_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ModuleListAdapter();
        RecyclerView recyclerView = view.findViewById(R.id.album_list);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory()).get(ModulesViewModel.class);
        mViewModel.getAllItems().observe(this, items -> {
            if(null == items)return;
            mAdapter.setModules(items);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.refreshModules();
    }
}
