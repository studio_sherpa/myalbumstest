package com.sherpastudio.myalbums.modules.domain;

import com.sherpastudio.myalbums.modules.data.IDataCallbackGetAllModules;
import com.sherpastudio.myalbums.modules.data.IDataRepository;
import com.sherpastudio.myalbums.modules.data.model.Module;
import com.sherpastudio.myalbums.modules.domain.base.UseCase;
import com.sherpastudio.myalbums.modules.view.ModuleItem;

import java.util.ArrayList;
import java.util.List;

public class UseCaseGetAllModels extends UseCase<UseCaseGetAllModels.RequestValues, UseCaseGetAllModels.ResponseValue> {

    private final IDataRepository mDataRepository;

    public UseCaseGetAllModels(IDataRepository dataRepository) {
        this.mDataRepository = dataRepository;
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        mDataRepository.getAllModules(new IDataCallbackGetAllModules() {
            @Override
            public void onSuccess(List<Module> modules) {
                ArrayList<ModuleItem> moduleItems = new ArrayList<>();
                for(int i = 0; i < modules.size(); i++){
                    moduleItems.add(new ModuleItem(modules.get(i), false));
                }
                ResponseValue responseValue = new ResponseValue(moduleItems);
                useCaseCallback.onSuccess(responseValue);
            }

            @Override
            public void onError(Throwable t) {
                // Never use generic exceptions. Create proper exceptions. Since
                // our use case is different we will go with generic throwable
                useCaseCallback.onError(new Throwable("Data not found"));
            }
        });
    }


    public static class RequestValues implements UseCase.RequestValues{

        public RequestValues() {}
    }

    public static class ResponseValue implements UseCase.ResponseValue{
        public final List<ModuleItem> list;

        ResponseValue(List<ModuleItem> list) {
            this.list = list;
        }
    }

}