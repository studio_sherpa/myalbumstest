package com.sherpastudio.myalbums.modules.view;

import com.sherpastudio.myalbums.modules.Injection;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ModulesViewModel.class) {
            return (T) new ModulesViewModel(
                    Injection.provideUseCaseHandler(), Injection.provideGetAllModels());
        }
        throw new IllegalArgumentException("unknown model class " + modelClass);
    }

    private ViewModelFactory(){

    }

    private static ViewModelFactory INSTANCE;
    public static ViewModelFactory getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ViewModelFactory();
        }
        return INSTANCE;
    }
}
