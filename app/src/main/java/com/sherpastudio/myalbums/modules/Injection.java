package com.sherpastudio.myalbums.modules;

import com.sherpastudio.myalbums.modules.data.DataRepository;
import com.sherpastudio.myalbums.modules.data.IDataRepository;
import com.sherpastudio.myalbums.modules.data.ILocalRepository;
import com.sherpastudio.myalbums.modules.data.IRemoteDataRepository;
import com.sherpastudio.myalbums.modules.data.MyMockLocalDataRepository;
import com.sherpastudio.myalbums.modules.data.MyRemoteDataRepository;
import com.sherpastudio.myalbums.modules.domain.UseCaseGetAllModels;
import com.sherpastudio.myalbums.modules.domain.base.UseCaseHandler;
import com.sherpastudio.myalbums.modules.view.ViewModelFactory;

public class Injection {

    public static IDataRepository provideDataRepository() {
        return DataRepository.getInstance(provideLocalDataRepository(), provideRemoteDataRepository());
    }

    public static ILocalRepository provideLocalDataRepository() {
        return MyMockLocalDataRepository.getInstance();
    }


    public static IRemoteDataRepository provideRemoteDataRepository() {
        return MyRemoteDataRepository.getInstance();
    }


    public static ViewModelFactory provideViewModelFactory() {
        return ViewModelFactory.getInstance();
    }

    public static UseCaseGetAllModels provideGetAllModels() {
        return new UseCaseGetAllModels(provideDataRepository());
    }

    public static UseCaseHandler provideUseCaseHandler(){
        return UseCaseHandler.getInstance();
    }
}