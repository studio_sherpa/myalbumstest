package com.sherpastudio.myalbums.modules.data;

import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.List;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyRemoteDataRepository implements IRemoteDataRepository{

    private final String ALBUMS_URL = "https://jsonplaceholder.typicode.com";


    private ModulesAPIService apiService;

    private static final MyRemoteDataRepository ourInstance = new MyRemoteDataRepository();

    public static MyRemoteDataRepository getInstance() {
        return ourInstance;
    }

    private MyRemoteDataRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ALBUMS_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(ModulesAPIService.class);
    }

    @Override
    public void getAllModules(@NonNull IDataCallbackGetAllModules callback) {
        apiService.listAllModules()
                .enqueue(new Callback<List<Module>>()
                {
                    @Override
                    public void onResponse(Call<List<Module>> call,
                                           Response<List<Module>> response)
                    {
                        if (response.isSuccessful())
                        {
                            callback.onSuccess(response.body());
                        }
                        else{
                            callback.onError(new Throwable(String.valueOf(response.code())));
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Module>> call, Throwable t) {
                        callback.onError(t);
                    }
                });
    }
}
