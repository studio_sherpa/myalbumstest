package com.sherpastudio.myalbums.modules.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Module {

    @SerializedName("title")
    @Expose
    private final String mTitle;
    @SerializedName("id")
    @Expose
    private final int mId;

    public Module(String mTitle, int mId) {
        this.mTitle = mTitle;
        this.mId = mId;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }
}
