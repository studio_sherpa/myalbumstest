package com.sherpastudio.myalbums.modules.data;


import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ModulesAPIService {
    @GET("albums")
    Call<List<Module>> listAllModules();

    @GET("albums/{id}")
    Call<Module> oneModule(@Path("id") String id);

}

