package com.sherpastudio.myalbums.modules.data;

import com.sherpastudio.myalbums.modules.data.model.Module;

import java.util.List;

public interface IDataCallbackGetAllModules {
    void onSuccess(List<Module> modules);
    void onError(Throwable throwable);
}
